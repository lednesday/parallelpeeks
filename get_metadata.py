"""
    Description: Prompts the user to provide metadata about the file to be produced,
    and to select which tiers should be used for original and translation tiers

    Todo: This really should have a GUI with more functionality: ability for the user
    to add formatting or at least manually enter a few html tags, visual selection
    of tiers to include, preview of information to be included, and ability to merge
    "chunks" into larger chunks if desired
    """

from bs4 import BeautifulSoup


def get_metadata(soup: BeautifulSoup) -> dict:
    """
    Parameter: soup is a BeautifulSoup object representing the original XML file

    Returns: metadata_variables is a dictionary containing the following keyword/value paris:
        title is the title of the text that will appear in the DOM body
        metadata is information that may include who's on the recording, what they're
        talking about, date and location of the recording, transcription/translation/
        annotation information, and funding sources
        dom_title is the title of the html document for head block
        dom_description is the description of the html document for the head block
        original_tier is a list; each element contains one "chunk" of text in the original
        language
        translation_tier is another list whose indexing matches original_tier's; each element
        contains one matching "chunk" of translation
    """

    metadata_variables = {'soup': soup}
    # extract draft title from name of primary media file
    media_descriptor = soup.media_descriptor['extracted_from']
    title = media_descriptor.rpartition('/')[2]
    # show user draft title and prompt for new title
    print("Working title: ", title)
    change_title = input("Would you like to change this title (y/n)? ")
    while(change_title == 'y'):
        title = input("Type the name of the new title. ")
        print("Revised title: ", title)
        change_title = input(
            "Would you like to change this title again (y/n)? ")
    metadata_variables.update({'title': title})

    # detect tiers and prompt user for which tiers to use
    # find all the tiers and get their titles based on tier_id
    tier_titles = []
    for tier in soup.find_all('tier'):
        tier_titles.append(tier['tier_id'])
    # display found tiers for user
    print("The following tiers were found:")
    print(tier_titles)
    print("You can select one original language tier and one translation tier.")
    for tier in tier_titles:
        use_tier = input("Would you like to include " + tier +
                         " in your parallel text (y/n)? ")
        if(use_tier == 'y'):
            tier_type = input(
                "Is this in the original language (o) or a translation (t)? ")
            if(tier_type == 'o'):
                original_tier = tier
            else:
                translation_tier = tier
    print("original_tier: ", original_tier)
    print("translation_tier: ", translation_tier)
    # confirm tiers (allow for merges)
    metadata_variables.update({'original_tier': original_tier})
    metadata_variables.update({'translation_tier': translation_tier})

    # prompt user for metadata: speakers, location, date, annotator, funding, distribution
    metadata = input("What information would you like to display under the title of this text? This includes metadata such as speaker(s), location, date, annotator(s), funding sources, and distribution/attribution permissions) ")
    metadata_variables.update({'metadata': metadata})

    # prompt user for filename (use title as working filename)
    dom_title = title
    print("The name of the html document is currently ", title, ".")
    change_dom_title = input(
        "Would you like a different name for the html title (y/n)? ")
    if(change_dom_title == 'y'):
        dom_title = input("Enter a new name for the html title: ")
    metadata_variables.update({'dom_title': dom_title})

    dom_description = input(
        "Enter a short description of the text for the html description: ")
    metadata_variables.update({'dom_description': dom_description})
    return metadata_variables
