#!/usr/bin/env python3

from generateDOM import init_template, render_template
from gui import GetInfo
import tkinter


def plaintext_make_chunk_arrays(metadata_variables: dict, o_filepath: str, t_filepath: str) -> dict:
    """Makes two lists, one in original language and one in translation language, from plain text files that are line-break aligned.

    Args:
        metadata_variables (dict): dictionary of metadata variables for constructing the DOM

    Returns:
        metadata_variables adds original_chunks and translation_chunks lists to the dict
    """

    original_chunks = []
    translation_chunks = []

    with open(o_filepath) as fp:
        file_contents = fp.readlines()
        for line in file_contents:
            chunk = line.strip('\n')
            if chunk != '':
                original_chunks.append(chunk)

    with open(t_filepath) as fp:
        file_contents = fp.readlines()
        for line in file_contents:
            chunk = line.strip('\n')
            if chunk != '':
                translation_chunks.append(chunk)

    metadata_variables.update(
        {'original_chunks': original_chunks, 'translation_chunks': translation_chunks})

    return metadata_variables


def main():
    root = tkinter.Tk()
    info = GetInfo(root)
    root.mainloop()
    metadata_variables = info.metadata_variables
    o_filepath = 'plain_texts/portuguese.txt'
    t_filepath = 'plain_texts/english.txt'
    metadata_variables = plaintext_make_chunk_arrays(
        metadata_variables, o_filepath, t_filepath)
    # use Jinja2 to write the DOM
    template_env = init_template('templates')
    render_template(template_env, 'parallelTemplate.html.jinja',
                    metadata_variables)


if __name__ == '__main__':
    main()
