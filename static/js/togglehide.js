// vtranslate: 
//   Demonstration of modifying element visibility 
//   by handling key clicks in javascript. 
// 
function toggle_translation(target_chunk) {
    console.log("Commencing toggle");
    // The 'this' object is set automatically in event handlers
    target_chunk.classList.toggle("translated");
    var sib = target_chunk.nextElementSibling;
    // The very next element should be the english translation,
    // but we'll search for it just in case. 
    while (sib) {
        console.log("Checking " + sib);
        var css_classes = sib.classList;
        if (css_classes.contains("en")) {
            css_classes.toggle("hide");
            console.log("Toggled!");
            return;
        }
        sib = sib.nextElementSibling;
    }
    alert("Didn't find translation");
    return;
}

function click_listener(event) {
    console.log("Click noted on " + this);
    toggle_translation(this);
}

function keyboard_listener(event) {
    console.log("Keypress noted on " + this);
    let enter_key = 13;
    if (event.keyCode == enter_key) {
        toggle_translation(this)
    }
}

document.addEventListener("DOMContentLoaded", function () {
    let targ_elements = document.querySelectorAll(".targ");
    for (let i = 0; i < targ_elements.length; ++i) {
        targ_elements[i].addEventListener('click', click_listener);
        targ_elements[i].addEventListener('keydown', keyboard_listener);
    }
});


