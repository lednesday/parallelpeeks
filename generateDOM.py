#!/usr/bin/env python3
"""

Description: Generates a DOM in html from an XML file produced by ELAN
(https://archive.mpi.nl/tla/elan)
The html file, with the help of separately-written css and js files, presents a text
in the original language; when a "chunk" is clicked, its translation (as given in
the XML file) is presented below.

DOM is written to templates file as a side effect of the render_template method.

Author: Lindsay Marean (with lots of help from Michal Young)

"""

# pathname for bs4 only works in virtual environment
from bs4 import BeautifulSoup
import jinja2
from jinja2 import Environment, FileSystemLoader, select_autoescape
# magical incantation from here: https://jinja.palletsprojects.com/en/2.11.x/api/
from get_metadata import *


def get_file(filepath: str) -> BeautifulSoup:
    """
    Description: opens the XML file, uses BeautifulSoup
    (https://www.crummy.com/software/BeautifulSoup/bs4/doc/) to create a
    BeautifulSoup object

    Parameters: filepath is the path and name of the XML document to be converted

    Returns: soup is a BeautifulSoup object created from the original XML document

    Todo: can I use Python's own XLM module instead of BeautifulSoup?
    Todo: allow the user to specify the filename and path
    """
    # get the file to be parsed
    with open(filepath) as fp:
        soup = BeautifulSoup(fp, features="html.parser")
    return soup


def make_chunk_arrays(metadata_variables: dict) -> dict:
    """
    Description: Makes two lists, one containing chunks in the original language, and one containing
    index-aligned translation chunks. Appends these two lists to the metadata_variables dictionary.

    Argument: metadata_variables is a dictionary with mix of user-provided information to be used
    in assembling the DOM

    Returns: metadata_variables, with original_chunks and translation_chunks appended
    """

    # put original and translation into arrays with matching indices
    # then I can go through and build the dom
    # but I need to do this with jinja

    original_chunks = []
    translation_chunks = []
    chunk_index = []

    original_tier = metadata_variables['original_tier']
    translation_tier = metadata_variables['translation_tier']

    for annotationO in metadata_variables['soup'].find(tier_id=original_tier).find_all('annotation'):
        # make an array of unique identifiers for the chunk pairs for future use
        index = annotationO.alignable_annotation['annotation_id']
        chunk_index.append(int(index.lstrip('a')))
        # makes an array of Tags
        full_tier = (annotationO.find('annotation_value'))
        # convert to string, strip tag labels, append to array
        original_chunks.append(str(full_tier).partition('>')[
            2].partition('<')[0])

    # Now do the same thing for translation_chunks
    for annotationT in metadata_variables['soup'].find(tier_id=translation_tier).find_all('annotation'):
        full_tier = (annotationT.find('annotation_value'))
        translation_chunks.append(str(full_tier).partition('>')[
            2].partition('<')[0])

        metadata_variables.update({'original_chunks': original_chunks})
        metadata_variables.update({'translation_chunks': translation_chunks})

    # Do I need to return metadata_variables for scope reasons?
    return metadata_variables


def init_template(boilerplate: str) -> Environment:
    """
    Description: Sets up the Jinja2 environment
    (https://jinja.palletsprojects.com/en/2.11.x/api/#jinja2.Environment)
    Copied from Michal's method in RUSA project
    Changes: path to template is passed in as a parameter

    Parameter: boilerplate is a path to the Jinja2 template

    Returns: template_env object to be used in rendering templates
    """

    template_loader = jinja2.FileSystemLoader(searchpath=boilerplate)
    template_env = jinja2.Environment(
        loader=template_loader, lstrip_blocks=True)
    return template_env


def render_template(template_env: Environment, inFilename: str, metadata_variables: dict) -> None:
    """
    Description: Writes a new html document by rendering a Jinja2 template
    copied from Michal's code
    """
    template = template_env.get_template(inFilename)
    # should check to make sure get_template worked - if it didn't TemplateNotFound
    # exception is raised
    outputText = template.render(metadata_variables)

    # write the DOM
    filename = metadata_variables['dom_title']
    filename += '.html'
    dom = open("texts/"+filename, 'w')
    dom.write(outputText)

    dom.close()


def main():
    # make a BeautifulSoup object
    soup = get_file("ELAN_files/2016-01-14-03.eaf")
    # get metadata information from user
    metadata_variables = get_metadata(soup)
    # uncomment to derive original and translation lists from soup
    metadata_variables = make_chunk_arrays(metadata_variables)
    # use Jinja2 to write the DOM
    template_env = init_template('templates')
    render_template(template_env, 'parallelTemplate.html.jinja',
                    metadata_variables)


if __name__ == '__main__':
    main()
