#!/usr/bin/env python3

import tkinter
from tkinter import ttk
from bs4 import BeautifulSoup
from generateDOM import get_file


class GetInfo:

    def __init__(self, master: tkinter):

        master.title('Parallel Peeks Document Information')
        self.metadata_variables = {}

        # frame for title and metadata fields
        self.visible_info = ttk.Frame(master)
        self.visible_info.pack()

        # title
        ttk.Label(self.visible_info,
                  text='Enter a title to be displayed with the text:').grid(row=0, column=0)
        self.title_entry = ttk.Entry(self.visible_info, width=50)
        self.title_entry.grid(row=1, column=0)

        # metadata info
        ttk.Label(self.visible_info, text=('Enter information about the text that you would like displayed,'
                                           'such as speaker(s), location, date, annotator(s), '
                                           'funding sources, and distribution/attribution permissions'), wraplength=300).grid(row=2, column=0)
        self.metadata_entry = tkinter.Text(
            self.visible_info, width=50, height=10)
        self.metadata_entry.grid(row=3, column=0)
        self.metadata_entry.config(wrap='word')

        # # frame for original and translation tier selection
        # self.tier_selection = ttk.Frame(master)
        # self.tier_selection.pack()

        # # This doesn't seem right, to put this function definition here
        # def get_tier_titles(soup: BeautifulSoup) -> list:

        #     tier_titles = []
        #     for tier in soup.find_all('tier'):
        #         tier_titles.append(tier['tier_id'])
        #     return tier_titles

        # tier_titles = get_tier_titles(metadata_variables['soup'])

        # # original tier
        # ttk.Label(self.tier_selection,
        #           text='Select the ELAN tier that contains the lines of text in the target language:').pack()
        # original_tier = tkinter.StringVar()
        # o_tier_button = []
        # for index, tier in enumerate(tier_titles):
        #     o_tier_button.append(tkinter.Radiobutton(
        #         self.tier_selection, text=tier, variable=original_tier, value=tier).pack())
        # metadata_variables.update({'original_tier': original_tier.get()})

        # # translation tier
        # ttk.Label(self.tier_selection,
        #           text='Select the ELAN tier that contains the translation of each line:').pack()
        # translation_tier = tkinter.StringVar()
        # t_tier_button = []
        # for index, tier in enumerate(tier_titles):
        #     t_tier_button.append(tkinter.Radiobutton(
        #         self.tier_selection, text=tier, variable=translation_tier, value=tier).pack())
        # metadata_variables.update({'translation_tier': translation_tier.get()})

        # frame for dom title and description
        self.dom_info = ttk.Frame(master)
        self.dom_info.pack()
        ttk.Label(self.dom_info, text=(
            'The html encoding of your webpage will have some additional information, '
            'not visible on the page, about the contents of the page. '
            'The two fields below allow you to enter this information.'), wraplength=300
        ).grid(row=0, column=0)

        # DOM title
        ttk.Label(self.dom_info, text='Enter a title for this page:').grid(
            row=1, column=0)
        self.dom_title_entry = ttk.Entry(self.dom_info, width=50)
        self.dom_title_entry.grid(row=2, column=0)
        # figure out how to have default text be title

        # DOM description
        ttk.Label(self.dom_info, text='Enter a short description of this page:').grid(
            row=3, column=0)
        self.desription_entry = ttk.Entry(self.dom_info, width=50)
        self.desription_entry.grid(row=4, column=0)

        # frame for submit
        self.submit_zone = ttk.Frame(master)
        self.submit_zone.pack()

        ttk.Label(self.submit_zone, text='Click "Submit" when you\'re ready').grid(
            row=0, column=0)
        submit = ttk.Button(self.submit_zone, text='Submit',
                            command=self.fill_metadata)
        submit.grid(row=1, column=0)

    def fill_metadata(self):
        self.metadata_variables.update({'title': self.title_entry.get()})
        self.metadata_variables.update(
            {'metadata': self.metadata_entry.get(1.0, 'end').strip()})
        self.metadata_variables.update(
            {'dom_title': self.dom_title_entry.get()})
        self.metadata_variables.update(
            {'dom_description': self.desription_entry.get()})


def main() -> dict:
    # def main(soup: BeautifulSoup) -> dict:
    # soup = get_file("2016-01-14-03.eaf")
    # metadata_variables = {'soup': soup}
    root = tkinter.Tk()
    info = GetInfo(root)
    root.mainloop()
    print(info.metadata_variables)


if __name__ == "__main__":
    main()
